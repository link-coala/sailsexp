hace 4 meses
hace 4 meses
hace 4 meses
hace 4 meses
hace 4 meses
hace 4 meses
hace 4 meses

/**
* Emoji.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    // e.g. ":("
    text: {
      type: 'string'
    },

    // every Emoji record belongs to a particular user.
    owner: {
      model: 'User'
    },

    leastFavoriteOf: {
      collection: 'User',
      via: 'leastFavoriteEmojis'
    }

  }
};

